<?php

namespace Drupal\Tests\update_narf\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that settings page loads with module enabled.
 *
 * @group update_narf
 */
class LoadTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'update',
    'update_narf',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the settings and expunging forms load with a 200 response.
   */
  public function testLoad() {
    $this->drupalGet('admin/reports/updates/narf');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('admin/reports/updates/narf/advanced');
    $this->assertSession()->statusCodeEquals(200);
  }

}
