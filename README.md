# Update NARF!

**Squish those pesky "No available releases found" issues.**

The update manager sometimes ends up with "No available release found" (NARF!)
for all, or just a couple of the modules in a site. This is an irritation and
potential security issue as an admin will not be able to see if there is a new
security release available for a module.

There are a number of attempts to fix this issue but none seem to be a permanent
fix that works for everyone. Some of the details are available in issue
[#2920285: Update module can get 'stuck' with 'no releases available'](https://www.drupal.org/project/drupal/issues/2920285).

This module just steps in and deletes the extraneous update_fetch_task just
prior to the update module checking for and creating them again.

It will be deprecated for newer versions of Drupal Core once the bug is
squashed.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/update_narf).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/update_narf).


## Requirements

This module requires the following core modules:

- Update Manager (drupal:update)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

If you find your site cannot complete the normal Update Manager via cron inside
the default 2 hour window, you may wish to increase the expiry time given to
update_fetch_task items at admin/reports/updates/narf.


## Usage

Operation of this module is for the most part automatic. An admin can cause the
orphans to be immediately removed by checking for updates manually.

Orphan update_fetch_task's are removed in a number of situations:
- Upon manual installation of this module, all orphans are removed.
- On cron run, just before update_cron() will do a full check.
- On user initiated "Check manually" for updates.
- On cron run, 2 hours after a update_fetch_task is first seen by this module.

The 2 hour expiry is configurable.

### Advanced Data Expunge & Rebuild

Linked from the NARF! Setting page, the Advanced Data Expunging form allows an
admin to review the state, keyvalue, and queue status used by the Update module.

It also allows for all of these data to be deleted to possibly remove any
impediments that the Update module is running into, and also optionally rebuild
that data using an alternative batch method.

The data presented may allow an admin to identify why they are still getting
NARFs showing up on their site. If a pattern or a reason can be ascertained as
to why that is happening, posting to either the [#2920295] issue, or the [issue queue](https://www.drupal.org/project/issues/update_narf) on this module could help towards
solving this annoying bug.

## Troubleshooting

Instead of using this module, you can delete the keyvalue collection yourself:

<code>drush php:eval "\Drupal::keyValue('update_fetch_task')->deleteAll();"</code>
