<?php

/**
 * @file
 * Fixes the "No available releases found" with the Core Update Manager module.
 *
 * This module steps in just before Update Manager checks for update_fetch_task
 * key_value and removes the stale ones to avoid a release check getting stuck.
 */

use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function update_narf_help($route_name, RouteMatchInterface $route_match) {

  static $helpful_routes = [
    'help.page.update_narf',
    'update_narf.settings',
  ];
  if (in_array($route_name, $helpful_routes)) {
    return implode("\n", [
      '<h3>', new TranslatableMarkup('About Update NARF!'), '</h3>',
      '<p><em><strong>', new TranslatableMarkup('Squish those pesky "No available releases found" issues.'), '</strong></em></p>',
      '<p>', new TranslatableMarkup('The update manager sometimes ends up with "No available release found" (NARF!) for all, or just a couple of the modules in a site. This is an irritation and potential security issue as an admin will not be able to see if there is a new security release available for a module.'), '</p>',
      '<p>', new TranslatableMarkup('There are a number of attempts to fix this issue but none seem to be a permanent fix that works for everyone. Some of the details are available in issue @issue_link.', [
        '@issue_link' => Link::fromTextAndUrl(
          "#2920285: Update module can get 'stuck' with 'no releases available'",
          Url::fromUri(
            'https://www.drupal.org/i/2920285', [
              'absolute' => TRUE,
              'https' => TRUE,
            ]
          ),
        )->toString(),
      ]), '</p>',
      '<p>', new TranslatableMarkup('This module just steps in and deletes the extraneous update_fetch_task just prior to the update module checking for and creating them again.'), '</p>',
    ]);
  }
}

/**
 * Implements hook_config_schema_info_alter().
 */
function update_narf_config_schema_info_alter(&$definitions) {
  // Add in fully validatable for >=D10.3.
  // @see https://www.drupal.org/node/3404425
  if (version_compare(\Drupal::VERSION, '10.3', '>=')) {
    if (isset($definitions['update_narf.settings'])) {
      $definitions['update_narf.settings']['constraints']['FullyValidatable'] = NULL;
    }
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function update_narf_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'cron') {
    // Move update_narf to before core's update module.
    $group = $implementations['update_narf'];
    unset($implementations['update_narf']);
    $implementations_keys = array_keys($implementations);
    $update_pos = array_search('update', $implementations_keys, TRUE);
    $implementations
      = array_slice($implementations, 0, $update_pos, TRUE)
      + ['update_narf' => $group]
      + array_slice($implementations, $update_pos, NULL, TRUE);
  }
}

/**
 * Implements hook_cron().
 */
function update_narf_cron() {
  // Update NARF!'s hook_cron has been moved to run just before update_cron().
  // Check the same status values to see if it will run, and remove orphan data
  // before it gets in the way of that processing.
  // @see update_cron().
  $update_config = \Drupal::config('update.settings');
  $frequency = $update_config->get('check.interval_days');
  $interval = 60 * 60 * 24 * $frequency;
  $last_check = \Drupal::state()->get('update.last_check', 0);
  $request_time = \Drupal::time()->getRequestTime();
  $difference = $request_time - $last_check;
  if ($difference > $interval) {
    // Could check to see if update_fetch_tasks queue has items in it and not
    // delete anything if it does, but it really shouldn't still be running if
    // the check.interval_days (DAYS!) has expired. Forced flush.
    \Drupal::service('update_narf.manager')->flushUpdateTasks(TRUE);
  }
  // Update the first seen every 15 minutes and remove any expired.
  elseif ($difference > 900) {
    \Drupal::service('update_narf.manager')->expireTasks();
  }
}

/**
 * Implements hook_modules_installed().
 */
function update_narf_modules_installed($modules, $is_syncing) {
  // A user will probably have a number of NARF! showing when installing this
  // module manually, so clean up existing update_fetch_tasks to start from a
  // clean slate. Unlikely during config import.
  if (!$is_syncing && in_array('update_narf', $modules)) {
    \Drupal::service('update_narf.manager')->flushUpdateTasks(TRUE);
  }
}
