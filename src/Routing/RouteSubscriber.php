<?php

namespace Drupal\update_narf\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Wrap the Update Manager's controller routes with UpdateNarfController.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // Replace controller on Available updates page.
    if ($route = $collection->get('update.status')) {
      $route->setDefault(
        '_controller',
        '\Drupal\update_narf\Controller\UpdateNarfController::updateStatus'
      );
    }

    // Replace controller on Manual update check.
    if ($route = $collection->get('update.manual_status')) {
      $route->setDefault(
        '_controller',
        '\Drupal\update_narf\Controller\UpdateNarfController::updateStatusManually'
      );
    }
  }

}
