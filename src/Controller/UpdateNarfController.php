<?php

namespace Drupal\update_narf\Controller;

use Drupal\Core\Render\RendererInterface;
use Drupal\update\Controller\UpdateController;
use Drupal\update\UpdateManagerInterface;
use Drupal\update_narf\UpdateNarfManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide wrappers for Core's Update Manager controller to fix NARF!
 */
class UpdateNarfController extends UpdateController {

  /**
   * The Update NARF! Manager.
   *
   * @var \Drupal\update_narf\UpdateNarfManagerInterface
   */
  protected $narfedManager;

  /**
   * Constructs an update narf controller.
   *
   * @param \Drupal\update\UpdateManagerInterface $update_manager
   *   Update Manager Service.
   * @param \Drupal\update_narf\UpdateNarfManagerInterface $update_narf_manager
   *   The Update NARF! Manager service.
   * @param \Drupal\Core\Render\RendererInterface|null $renderer
   *   The renderer.
   *
   * @phpcs:disable Drupal.Functions.MultiLineFunctionDeclaration.MissingTrailingComma
   */
  public function __construct(
    UpdateManagerInterface $update_manager,
    UpdateNarfManagerInterface $update_narf_manager,
    ?RendererInterface $renderer = NULL
  ) {
    // phpcs:enable
    $this->narfedManager = $update_narf_manager;

    parent::__construct($update_manager, $renderer);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('update.manager'),
      $container->get('update_narf.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Soft clean out orphan tasks before showing the available updates page.
   *
   * @return array
   *   A build array with the update status of projects.
   */
  public function updateStatus() {
    $this->narfedManager->flushUpdateTasks();

    return parent::updateStatus();
  }

  /**
   * Hard clean out orphan tasks before manually running updates checks.
   */
  public function updateStatusManually() {
    $this->narfedManager->flushUpdateTasks(TRUE);

    return parent::updateStatusManually();
  }

}
