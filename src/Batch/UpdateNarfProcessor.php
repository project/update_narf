<?php

namespace Drupal\update_narf\Batch;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Utility\ProjectInfo;
use Drupal\update\UpdateManagerInterface;

/**
 * Setup and process the operation to rebuild project update data.
 */
class UpdateNarfProcessor {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The non-expirable key/value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValueFactory;

  /**
   * The expirable key/value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValueExpirableFactory;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Module Handler Service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The theme extension list.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected $themeExtensionList;

  /**
   * Update manager service.
   *
   * @var \Drupal\update\UpdateManagerInterface
   */
  protected $updateManager;

  /**
   * The update fetch queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $fetchQueue;

  /**
   * Update Fetch Task Store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $fetchTaskStore;

  /**
   * Constructs a new UpdateNarfProcessor object.
   *
   * NB: Do not convert to property promotion or autowire due to this module
   * supporting Drupal back to 8.7.7.
   *
   * @phpcs:disable Drupal.Functions.MultiLineFunctionDeclaration.MissingTrailingComma
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    KeyValueFactoryInterface $key_value_factory,
    KeyValueFactoryInterface $key_value_expirable_factory,
    ModuleExtensionList $extension_list_module,
    ModuleHandlerInterface $module_handler,
    QueueFactory $queue_factory,
    ThemeExtensionList $extension_list_theme,
    TranslationInterface $string_translation,
    UpdateManagerInterface $update_manager
  ) {
    // phpcs:enable
    $this->configFactory = $config_factory;
    $this->keyValueFactory = $key_value_factory;
    $this->keyValueExpirableFactory = $key_value_expirable_factory;
    $this->moduleExtensionList = $extension_list_module;
    $this->moduleHandler = $module_handler;
    $this->queueFactory = $queue_factory;
    $this->themeExtensionList = $extension_list_theme;
    $this->stringTranslation = $string_translation;
    $this->updateManager = $update_manager;

    // Create specific instances used multiple times.
    $this->fetchQueue = $this->queueFactory->get('update_fetch_tasks');
    $this->fetchTaskStore = $this->keyValueFactory->get('update_fetch_task');
  }

  /**
   * Build the batch array, set it to run, and return it.
   *
   * @param bool $set
   *   If TRUE this method will also call batch_set on the array.
   *
   * @return array
   *   The batch array built for the update.
   */
  public function batchBuildSet(bool $set = TRUE): array {
    $batch_builder = (new BatchBuilder())
      ->setTitle($this->t('Checking available update data'))
      ->addOperation([$this, 'buildExtensionList'], ['module'])
      ->addOperation([$this, 'buildExtensionList'], ['theme'])
      ->addOperation([$this, 'buildProjectsList'], [])
      ->addOperation([$this, 'enqueueFetchTasks'], [])
      ->addOperation([$this->updateManager, 'fetchDataBatch'], [])
      ->addOperation([$this, 'processCurrent'], [])
      ->setProgressMessage($this->t('Trying to check available update data ...'))
      ->setErrorMessage($this->t('Error checking available update data.'))
      ->setFinishCallback('update_fetch_data_finished');

    if ($set) {
      batch_set($batch_builder->toArray());
    }

    return $batch_builder->toArray();
  }

  /**
   * Batch callable to rebuild an extension list.
   *
   * This is normally done as part of UpdateManager::getProjects().
   *
   * @param string $type
   *   The extension list type to rebuild.
   * @param array $context
   *   The batch context array.
   */
  public function buildExtensionList($type, &$context): void {
    switch ($type) {
      case 'module':
        $this->moduleExtensionList->getList();
        break;

      case 'theme':
        $this->themeExtensionList->getList();
        break;

      default:
        throw new \Exception('Invalid extension type given');

    }
  }

  /**
   * Batch callable to build the projects list used by update module.
   *
   * This creates a new ProjectInfo with all of the modules and themes that the
   * update manager will fetch updates for.
   *
   * This is normally part of UpdateManager::getProjects().
   *
   * @param array $context
   *   The batch context array.
   */
  public function buildProjectsList(&$context): void {
    if (empty($context['sandbox']['max'])) {
      // Step step process.
      $context['sandbox']['step'] = 0;
      $context['sandbox']['max'] = 5;

      // To process uninstalled modules or not.
      $context['sandbox']['do_uninstalled'] = $this->configFactory
        ->get('update.settings')->get('check.disabled_extensions');

      // The ProjectInfo class.
      $context['sandbox']['project_info_object'] = new ProjectInfo();

      // The projects array.
      $context['sandbox']['projects'] = [];
    }
    $context['sandbox']['step']++;
    $step = $context['sandbox']['step'];

    // Step 1: Process installed module data.
    if ($step == 1) {
      $module_data = $this->moduleExtensionList->getList();
      $context['sandbox']['project_info_object']->processInfoList(
        $context['sandbox']['projects'],
        $module_data,
        'module',
        TRUE
      );
    }

    // Step 2: Process installed theme data.
    elseif ($step == 2) {
      $theme_data = $this->themeExtensionList->getList();
      $context['sandbox']['project_info_object']->processInfoList(
        $context['sandbox']['projects'],
        $theme_data,
        'theme',
        TRUE
      );
    }

    // Step 3: process uninstalled module data (configurable).
    elseif ($step == 3 && $context['sandbox']['do_uninstalled']) {
      $module_data = $this->moduleExtensionList->getList();
      $context['sandbox']['project_info_object']->processInfoList(
        $context['sandbox']['projects'],
        $module_data,
        'module',
        FALSE
      );
    }

    // Step 4: Process uninstalled theme data (configurable).
    elseif ($step == 4 && $context['sandbox']['do_uninstalled']) {
      $theme_data = $this->themeExtensionList->getList();
      $context['sandbox']['project_info_object']->processInfoList(
        $context['sandbox']['projects'],
        $theme_data,
        'theme',
        FALSE
      );
    }

    // Step 5: Alter and store the resulting project data.
    elseif ($context['sandbox']['step'] == 5) {
      // Allow modules the alter projects before further processing.
      $this->moduleHandler->alter(
        'update_projects',
        $context['sandbox']['projects']
      );

      // Store the site's project data for at most 1 hour.
      $this->keyValueExpirableFactory->get('update')->setWithExpire(
        'update_project_projects',
        $context['sandbox']['projects'],
        3600
      );

      // Now that the alter call has been made, delete the available releases
      // data so it can be refreshed.
      $releases_expirable = $this->keyValueExpirableFactory
        ->get('update_available_releases');
      $releases_expirable->deleteAll();
    }

    // Determine if finished.
    if ($context['sandbox']['step'] >= $context['sandbox']['max']) {
      $context['finished'] = 1;
      return;
    }
    $context['finished'] = $context['sandbox']['step'] / $context['sandbox']['max'];
  }

  /**
   * Batch callable to enqueue a fetch task for each known project.
   *
   * This is normally part of UpdateManager::refreshUpdateData().
   *
   * @param array $context
   *   The batch context array.
   */
  public function enqueueFetchTasks(&$context): void {
    // Clean out the queue and the semaphore store immediately before adding
    // to them again.
    $this->fetchTaskStore->deleteAll();
    $this->fetchQueue->deleteQueue();
    $this->fetchQueue->createQueue();

    // Get projects list directly.
    $projects = $this->keyValueExpirableFactory
      ->get('update')
      ->get('update_project_projects');
    if (empty($projects)) {
      throw new \Exception('Project data from previous step is missing');
    }

    // Duplicate the behaviour of UpdateProcessor::createFetchTask without the
    // pre-check on the semaphore since it was just deleted above.
    foreach ($projects as $project) {
      $this->fetchQueue->createItem($project);
      $this->fetchTaskStore->set($project['name'], $project);
    }
  }

  /**
   * Batch callback to process release data into current data.
   *
   * @param array $context
   *   The batch context array.
   */
  public function processCurrent(&$context): void {
    // Update available after the fetch task has run.
    $available = $this->keyValueExpirableFactory
      ->get('update_available_releases')
      ->getAll();

    // Process the update data.
    $this->moduleHandler->loadInclude('update', 'inc', 'update.compare');
    update_calculate_project_data($available);
  }

}
