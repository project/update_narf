<?php

namespace Drupal\update_narf;

/**
 * Locates and removes extraneous update_fetch_task keyvalue items.
 */
interface UpdateNarfManagerInterface {

  /**
   * Checks for extraneous update_fetch_task key value items and removes them.
   *
   * @param bool $force
   *   Force removal of orphan tasks even when conditions aren't quite right.
   */
  public function flushUpdateTasks($force = FALSE);

  /**
   * Remove any update_fetch_task items which have exceeded their lifespan.
   *
   * @param array|null $all_tasks
   *   Provide the list of all current tasks if available.
   */
  public function expireTasks($all_tasks = NULL);

  /**
   * Refresh the first seen keyvalue store for each update_fetch_task.
   *
   * @param array|null $all_tasks
   *   Provide the list of all current tasks if available.
   *
   * @return array
   *   An array of keyvalue keys which should be removed.
   */
  public function refreshSeen($all_tasks = NULL);

}
