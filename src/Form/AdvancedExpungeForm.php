<?php

namespace Drupal\update_narf\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\update_narf\Batch\UpdateNarfProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Advanced expunging options to fix drupal updates.
 */
class AdvancedExpungeForm extends FormBase {

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The non-expirable key/value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValueFactory;

  /**
   * The expirable key/value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValueExpirableFactory;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The theme extension list.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected $themeExtensionList;

  /**
   * Update NARF batch processor service.
   *
   * @var \Drupal\update_narf\Batch\UpdateNarfProcessor
   */
  protected $updateNarfProcessor;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'update_narf_nuclear_options';
  }

  /**
   * Constructs a new NuclearOptionsForm object.
   *
   * @phpcs:disable Drupal.Functions.MultiLineFunctionDeclaration.MissingTrailingComma
   */
  public function __construct(
    DateFormatterInterface $date_formatter,
    KeyValueFactoryInterface $key_value_factory,
    KeyValueFactoryInterface $key_value_expirable_factory,
    ModuleExtensionList $extension_list_module,
    QueueFactory $queue_factory,
    StateInterface $state,
    ThemeExtensionList $extension_list_theme,
    UpdateNarfProcessor $update_narf_processor
  ) {
    // phpcs:enable
    $this->dateFormatter = $date_formatter;
    $this->keyValueFactory = $key_value_factory;
    $this->keyValueExpirableFactory = $key_value_expirable_factory;
    $this->moduleExtensionList = $extension_list_module;
    $this->queueFactory = $queue_factory;
    $this->state = $state;
    $this->themeExtensionList = $extension_list_theme;
    $this->updateNarfProcessor = $update_narf_processor;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('keyvalue'),
      $container->get('keyvalue.expirable'),
      $container->get('extension.list.module'),
      $container->get('queue'),
      $container->get('state'),
      $container->get('extension.list.theme'),
      $container->get('update_narf.processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['info'] = [
      '#type' => 'fieldset',

      'details' => [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $this->t('The Update module state can still be left in an unworkable state, caused by a presently unknown set of circumstances. This page displays information about that internal state which may help to identify why that is, and the option to clear it all and optionally rebuild it using a batch process.'),
      ],

      'details_too' => [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $this->t('If you find can identify a pattern in the data which may be causing the NARF issues for you, please post it to @issue_link or as a new issue on @project_link as appropriate.', [
          '@issue_link' => Link::fromTextAndUrl(
            '#2920285',
            Url::fromUri(
              'https://www.drupal.org/i/2920285', [
                'absolute' => TRUE,
                'https' => TRUE,
              ]
            ),
          )->toString(),
          '@project_link' => Link::fromTextAndUrl(
            'Update NARF! issues',
            Url::fromUri(
              'https://www.drupal.org/project/issues/update_narf?categories=All', [
                'absolute' => TRUE,
                'https' => TRUE,
              ]
            ),
          )->toString(),
        ]),
      ],

      'details_tre' => [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $this->t('Expunging will clear out all state and queue data for the Update module and internal modules/themes lists. Also optionally rebuild that data and fetch available releases data.'),
      ],
    ];

    $form['rebuild'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Batch rebuild update module state after deletion.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Expunge Update State Data'),
        '#attributes' => [
          'class' => ['button--primary'],
        ],
      ],
      '#weight' => 5,
    ];

    // Data display.
    $form['params'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Update module current state data'),
      '#weight' => 10,
    ];
    $form['params']['table'] = [
      '#type' => 'table',
      '#header' => [
        'parameter' => $this->t('Parameter'),
        'value' => $this->t('Value'),
      ],
      '#rows' => [],
    ];

    // Update last run.
    $update_last_check = $this->state->get('update.last_check', 0);
    $form['params']['table']['#rows'][] = [
      $this->t('Update last checked'),
      ($update_last_check == 0)
        ? $this->t('Never')
        : $this->dateFormatter->formatTimeDiffSince($update_last_check),
    ];

    // Update last emailed.
    $update_last_email = $this->state->get('update.last_email_notification', 0);
    $form['params']['table']['#rows'][] = [
      $this->t('Update last emailed'),
      ($update_last_email == 0)
        ? $this->t('Never')
        : $this->dateFormatter->formatTimeDiffSince($update_last_email),
    ];

    // Items in the fetch queue.
    $queue = $this->queueFactory->get('update_fetch_tasks');
    $form['params']['table']['#rows'][] = [
      $this->t('Fetch queue count'),
      $queue_count = $queue->numberOfItems(),
    ];

    // Semaphore count.
    $update_forever = $this->keyValueFactory
      ->get('update_fetch_task');
    $all_semaphores = $update_forever->getAll();
    $form['params']['table']['#rows'][] = [
      $this->t('Queue semaphore count'),
      $semaphores_count = count($all_semaphores),
    ];

    // Imbalance in the force.
    if ($queue_count != $semaphores_count) {
      $form['params']['table']['#rows'][] = [
        [
          'colspan' => 2,
          'data' => [
            '#prefix' => '<strong>',
            '#suffix' => '</strong>',
            '#markup' => $this->t('Mismatch in queue/semaphore count. This is a known cause of NARFs, but it should already be handled by the default function of this module. Expunging the Update module state data will clear both of these items.'),
          ],
        ],
      ];
    }

    // Installed projects considered by the update manager.
    $update_data = $this->keyValueExpirableFactory
      ->get('update');
    $update_project_projects = $update_data->get('update_project_projects');
    $form['params']['table']['#rows'][] = [
      $this->t('Projects considered for update count'),
      $update_project_projects ? count($update_project_projects) : $this->t('null'),
    ];

    // Current update status of all projects considered by the update manager.
    $update_project_data = $update_data->get('update_project_data');
    $form['params']['table']['#rows'][] = [
      $this->t('Current update of projects count'),
      $update_project_data ? count($update_project_data) : $this->t('null'),
    ];

    // Available releases count.
    $releases_expirable = $this->keyValueExpirableFactory
      ->get('update_available_releases');
    $available_releases = $releases_expirable->getAll();
    $form['params']['table']['#rows'][] = [
      $this->t('Available releases count'),
      $available_releases ? count($available_releases) : $this->t('null'),
    ];

    // Modules.
    $form['modules'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Module list and available updates'),
      '#weight' => $form['params']['#weight'] + 1,
      '#description' => $this->t('<em>Version</em> is the current version as stated in the module info file. <em>Project data</em> is built from the info files. <em>Current data</em> is the current installed state as compared to the Release data. <em>Release data</em> is fetch from the release server. Only modules with <em>Available</em> in the Project data are considered by the Update module to fetch release data.'),
    ];
    $form['modules']['table'] = [
      '#type' => 'table',
      '#header' => [
        'module' => $this->t('Module'),
        'version' => $this->t('Version'),
        'project' => $this->t('Project data'),
        'update' => $this->t('Current data'),
        'release' => $this->t('Release data'),
      ],
      '#rows' => [],

      '#empty' => $this->t('No non-core modules available to site.'),
    ];

    // If Update module is configured to check for uninstalled modules, they
    // should be included in the list.
    $do_uninstalled = $this
      ->config('update.settings')->get('check.disabled_extensions');

    // Process the list of modules into the table.
    $module_list = $this->moduleExtensionList->getList();
    $module_paths = [];
    foreach ($module_list as $module_name => $module) {
      // Ignore core modules, except the first time when info is added.
      if ($module->origin == 'core') {
        if (!isset($form['modules']['table']['#rows']['core'])) {
          $project_info = $update_project_projects['drupal'] ?? NULL;
          $current_info = $update_project_data['drupal'] ?? NULL;
          $release_info = $available_releases['drupal'] ?? NULL;
          $form['modules']['table']['#rows']['core'] = [
            $this->t('Drupal Core'),
            $module->info['version'] ?? $this->t('dev'),
            $project_info ? $this->t('Available') : $this->t('not found'),
            $current_info ? $this->t('Available') : $this->t('not found'),
            $release_info ? $this->t('Available') : $this->t('not found'),
          ];
        }
        continue;
      }

      // Don't show modules that aren't installed unless update module is also
      // configured to look for them.
      if (!$do_uninstalled && !$module->status) {
        continue;
      }

      // Ignore modules that are sub-modules of other ones when they are a non
      // dev module which includes the version and project properties.
      $project = $module->info['project'] ?? FALSE;
      if ($project && isset($form['modules']['table']['#rows'][$project])) {
        continue;
      }

      // Ignore modules that are in a sub-directory of any previous modules.
      if (!$project && $this->isSubModule($module_paths, $module)) {
        continue;
      }
      $module_paths[] = $module->subpath;

      // Display data about the module.
      $project_info = $update_project_projects[$module->getName()] ?? NULL;
      $current_info = $update_project_data[$module->getName()] ?? NULL;
      $release_info = $available_releases[$module->getName()] ?? NULL;
      $is_dev = $module->info['version'] ?? TRUE;
      $form['modules']['table']['#rows'][$module_name] = [
        $module->info['name'],
        $is_dev === TRUE ? $this->t('dev') : $module->info['version'],
        $project_info
          ? $this->t('Available')
          : (($is_dev === TRUE) ? $this->t('n/a') : $this->t('not found')),
        $current_info
          ? $this->t('Available')
          : (($is_dev === TRUE) ? $this->t('n/a') : $this->t('not found')),
        $release_info
          ? $this->t('Available')
          : (($is_dev === TRUE) ? $this->t('n/a') : $this->t('not found')),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Noop.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $is_rebuilding = $form_state->getValue('rebuild');

    // Mimic uninstalling the Update module. If rebuilding, this is done
    // during the batched rebuild process.
    // @see update_uninstall().
    if (!$is_rebuilding) {
      $this->state->delete('update.last_check');
      $this->state->delete('update.last_email_notification');
      $queue = $this->queueFactory->get('update_fetch_tasks');
      $queue->deleteQueue();
    }

    // Remove additional state and kvp which are used in the update. In the
    // expirable key value store, this is update_projects_(projects|data).
    $update_expirable = $this->keyValueExpirableFactory
      ->get('update');
    $update_expirable->deleteAll();
    // Do not delete this now if doing a batch update.
    // @see UpdateManager::refreshUpdateData().
    if (!$is_rebuilding) {
      $releases_expirable = $this->keyValueExpirableFactory
        ->get('update_available_releases');
      $releases_expirable->deleteAll();
    }
    $update_forever = $this->keyValueFactory
      ->get('update_fetch_task');
    $update_forever->deleteAll();

    // Reset the extensions lists for modules and themes.
    $this->moduleExtensionList->reset();
    $this->themeExtensionList->reset();

    // Mimic installing the Update module. Again, not while rebuilding.
    // @see update_install().
    if (!$is_rebuilding) {
      $queue = $this->queueFactory->get('update_fetch_tasks', TRUE);
      $queue->createQueue();
    }

    $this->messenger()->addStatus('State and queued tasks expunged.');

    // Initiate a custom batch which precedes the manual update check with the
    // preparation steps which rebuild the extension lists etc.
    if ($is_rebuilding) {
      $this->updateNarfProcessor->batchBuildSet();
    }
  }

  /**
   * Return the available release information for the given module/theme.
   *
   * @param array $releases
   *   Available releases known to the site.
   * @param \Drupal\Core\Extension\Extension $extension
   *   The extension to return the specific release for.
   *
   * @return array|null
   *   Release information for the given extension or null.
   *
   * @phpcs:disable Drupal.Functions.MultiLineFunctionDeclaration.MissingTrailingComma
   */
  protected function getRelease(
    array &$releases,
    Extension $extension
  ): ?array {
    // phpcs:enable

    if (isset($releases[$extension->getName()])) {
      return $releases[$extension->getName()];
    }

    return NULL;
  }

  /**
   * Guess if a module is a sub-module of an already existing one.
   *
   * This function assumes that the containing module will be encountered
   * previously.
   *
   * @param array $module_paths
   *   Array of previously seen parent modules.
   * @param \Drupal\Core\Extension\Extension $extension
   *   The extension to determine if it's internal.
   *
   * @return bool
   *   Returns TRUE if the module is inside an already seen module.
   *
   * @phpcs:disable Drupal.Functions.MultiLineFunctionDeclaration.MissingTrailingComma
   */
  protected function isSubModule(
    array &$module_paths,
    Extension $extension
  ): bool {
    // phpcs:enable

    foreach ($module_paths as $module_path) {
      if (str_starts_with($extension->subpath, $module_path . '/')) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
