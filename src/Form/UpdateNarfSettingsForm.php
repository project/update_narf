<?php

namespace Drupal\update_narf\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure update NARF! settings for this site.
 */
class UpdateNarfSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new UpdateNarfSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config
   *   The typed config manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date related functionality.
   * @param \Drupal\Component\Datetime\TimeInterface $time_service
   *   The time service.
   *
   * @phpcs:disable Drupal.Functions.MultiLineFunctionDeclaration.MissingTrailingComma
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config,
    DateFormatterInterface $date_formatter,
    TimeInterface $time_service
  ) {
    // phpcs:enable
    // Starting with Drupal 10.2, include TypedConfigManagerInterface.
    // @see https://www.drupal.org/node/3404140
    if (version_compare(\Drupal::VERSION, '10.2', '>=')) {
      parent::__construct($config_factory, $typed_config);
    }
    else {
      // @phpstan-ignore-next-line (Only called for older versions of Drupal)
      parent::__construct($config_factory);
    }

    $this->dateFormatter = $date_formatter;
    $this->time = $time_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('date.formatter'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'update_narf_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'update_narf.settings',
    ];
  }

  /**
   * Exhaustively list components of a DateInterval instead of stopping.
   *
   * All Drupal core methods ignore further granularity once an empty level is
   * found, regardless of granularity option provided.
   *
   * @param int $from
   *   A UNIX timestamp, defining the from date and time.
   * @param int $to
   *   A UNIX timestamp, defining the to date and time.
   *
   * @return string
   *   A translated string representation of the interval. This interval is
   *   always positive. It includes all granularity details.
   */
  protected function exhaustivelyFormatDiff(int $from, int $to) {
    $from = DrupalDateTime::createFromTimestamp($from);
    $to = DrupalDateTime::createFromTimestamp($to);
    $interval = $from->diff($to);

    $output = '';
    $units = ['y', 'm', 'd', 'h', 'i', 's'];
    foreach ($units as $unit) {
      if ($interval->{$unit} > 0) {
        // Switch over the keys to call formatPlural() explicitly with literal
        // strings for all different possibilities.
        switch ($unit) {
          case 'y':
            $interval_output = $this
              ->formatPlural($interval->y, '1 year', '@count years');
            break;

          case 'm':
            $interval_output = $this
              ->formatPlural($interval->m, '1 month', '@count months');
            break;

          case 'd':
            $interval_output = $this
              ->formatPlural($interval->d, '1 day', '@count days');
            break;

          case 'h':
            $interval_output = $this
              ->formatPlural($interval->h, '1 hour', '@count hours');
            break;

          case 'i':
            $interval_output = $this
              ->formatPlural($interval->i, '1 minute', '@count minutes');
            break;

          case 's':
            $interval_output = $this
              ->formatPlural($interval->s, '1 second', '@count seconds');
        }
        $output .= ($output && $interval_output ? ' ' : '') . $interval_output;
      }
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('update_narf.settings');
    $task_expiry = $config->get('task_expiry') ?: (60 * 60 * 2);

    // Number of seconds to expiry to be updated.
    $form['task_expiry'] = [
      '#type' => 'value',
      '#value' => $task_expiry,
    ];

    // Convert seconds into strtotime input.
    $now = $this->time->getRequestTime();
    $then = $now + $task_expiry;
    $converted_expiry = $this->exhaustivelyFormatDiff($now, $then);

    // Provide user friendly input.
    $form['task_expiry_input'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Task expiry'),
      '#description' => $this->t('Period after which update_fetch_task items are removed from keyvalue store. Format should be compatible with PHP strtotime() and allow enough time for your site to complete update fetch tasks run by cron.'),
      '#default_value' => '+' . $converted_expiry,
    ];

    $return = parent::buildForm($form, $form_state);

    // Add link to nuclear options.
    $return['actions']['advanced'] = [
      '#type' => 'link',
      '#title' => $this->t('Advanced Data Expunging'),
      '#url' => Url::fromRoute('update_narf.advanced'),
      '#attributes' => [
        'class' => ['button'],
      ],
    ];

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // User input must be convertible into a positive number of seconds.
    $now = DrupalDateTime::createFromTimestamp($this->time->getRequestTime());
    $date = clone $now;
    try {
      $date->modify($form_state->getValue('task_expiry_input'));
    }
    // PHP >= 8.3.0 will throw DateMalformedStringException. Does not throw
    // any at present.
    catch (\Exception $e) {
      $form_state->setErrorByName('task_expiry_input', $e->getMessage());
      return;
    }

    $seconds = $date->getTimestamp() - $now->getTimestamp();
    if ($seconds <= 0) {
      $form_state->setErrorByName('task_expiry_input', $this->t(
        'Task expiry must result in a positive number of seconds. eg +2 hours'
      ));
      return;
    }

    // Modify the value to be saved.
    $form_state->setValue('task_expiry', $seconds);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Store value modified in validateForm().
    $this->config('update_narf.settings')
      ->set('task_expiry', $form_state->getValue('task_expiry'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
