<?php

namespace Drupal\update_narf;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Queue\QueueFactory;

/**
 * Default implementation of UpdateManagerInterface.
 */
class UpdateNarfManager implements UpdateNarfManagerInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Update Fetch Task Store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $fetchTaskStore;

  /**
   * Seen store for Update Fetch Tasks.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $taskSeenStore;

  /**
   * The module specific logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * The update fetch queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $fetchQueue;

  /**
   * Constructs a new UpdateNarfManager object.
   *
   * @phpcs:disable Drupal.Functions.MultiLineFunctionDeclaration.MissingTrailingComma
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TimeInterface $time,
    KeyValueFactoryInterface $key_value_factory,
    LoggerChannel $logger_channel,
    QueueFactory $queue_factory
  ) {
    // phpcs:enable
    $this->configFactory = $config_factory;
    $this->time = $time;
    $this->fetchTaskStore = $key_value_factory->get('update_fetch_task');
    $this->taskSeenStore = $key_value_factory->get('update_narf_fetch_task');
    $this->logger = $logger_channel;
    $this->fetchQueue = $queue_factory->get('update_fetch_tasks');
  }

  /**
   * {@inheritdoc}
   */
  public function flushUpdateTasks($force = FALSE) {

    // Do nothing more if the queue is not empty unless forced, at which time
    // the contents of the queue is deleted to prevent double-ups during this
    // upcoming check.
    if ($this->fetchQueue->numberOfItems() > 0) {
      if (!$force) {
        return;
      }

      $this->fetchQueue->deleteQueue();
    }

    // Check to see if there are any items in the collection.
    $all_tasks = $this->fetchTaskStore->getAll();
    if (empty($all_tasks)) {
      return;
    }

    // If forced, just delete them all and log the event.
    if ($force) {
      $all_tasks_modules = array_keys($all_tasks);
      $this->fetchTaskStore->deleteMultiple($all_tasks_modules);
      $this->taskSeenStore->deleteMultiple($all_tasks_modules);
      $this->logger->notice('Removed NARF: @modules', [
        '@modules' => implode(', ', $all_tasks_modules),
      ]);

      return;
    }

    // Expiry any tasks exceeding the configured lifespan.
    $this->expireTasks($all_tasks);
  }

  /**
   * {@inheritdoc}
   */
  public function expireTasks($all_tasks = NULL) {

    // Expire the expired tasks.
    if ($expired = $this->refreshSeen($all_tasks)) {
      $this->fetchTaskStore->deleteMultiple($expired);
      $this->taskSeenStore->deleteMultiple($expired);
      $this->logger->notice('Removed NARF: @modules', [
        '@modules' => implode(', ', $expired),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refreshSeen($all_tasks = NULL) {
    if (empty($all_tasks)) {
      $all_tasks = $this->fetchTaskStore->getAll();
    }
    $all_seen = $this->taskSeenStore->getAll();
    $all_tasks_modules = array_keys($all_tasks);
    $all_seen_modules = array_keys($all_seen);

    // Remove any keys which only exist in the last seen store.
    $unique_to_seen = array_diff(
      $all_seen_modules,
      $all_tasks_modules
    );
    $this->taskSeenStore->deleteMultiple($unique_to_seen);

    // Set seen for all tasks. Keep list of any expired.
    $now = $this->time->getRequestTime();
    $lifetime = $this->configFactory
      ->get('update_narf.settings')
      ->get('task_expiry') ?: 7200;
    $expire_time = $now - $lifetime;
    $expired = [];
    $seen_updates = [];
    foreach ($all_tasks as $module_name => $module_info) {
      if (isset($all_seen[$module_name])) {
        // Seen first over $lifetime seconds ago is expired.
        if ($all_seen[$module_name] < $expire_time) {
          $expired[] = $module_name;
        }
      }
      // Never seen before so start tracking it.
      else {
        $seen_updates[$module_name] = $now;
      }
    }
    if (!empty($seen_updates)) {
      $this->taskSeenStore->setMultiple($seen_updates);
    }

    return $expired;
  }

}
